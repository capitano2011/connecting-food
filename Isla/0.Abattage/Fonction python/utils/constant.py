from utils.utils import generate_output_filename

NULL = None
ACTOR_RECEIVER = "72262116600064"
SITE_RECEIVER = "Usine Duc"
INPUT_FILE_PATH = "/data/Template_extraction_Duc_220622.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
CATEGORY = "Abattage"
STEP = "processing"
