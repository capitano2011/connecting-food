import datetime
from pandas import DataFrame


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Template_extraction_Duc_220622 - " + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
    )
