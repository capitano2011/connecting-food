import os
import pandas as pd
from model.factory_reception import FactoryReception
from utils.constant import (
    PRODUCTS,
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    STEP,
    CATEGORY,
    LEVEL,
    RECEIVER_ACTOR_NUMBER,
    RECEIVER_UNIT,
    NULL,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):

    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", header=None)
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 15
    if column_number >= 15:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            receiver_product, sender_track_ids = row.get(2), row.get(5)

            # Do not take account the lines if receiver_product is in PRODUCTS
            if receiver_product.strip() not in PRODUCTS:

                # We create a specific line for every sender_track_id

                for sender_track_id in sender_track_ids.split("/"):

                    # Dict model

                    factory_dict = {
                        "category": CATEGORY,
                        "step": STEP,
                        "level": LEVEL,
                        "capture_start": row.get(0),
                        "capture_end": NULL,
                        "capture_uid": NULL,
                        "sender_actor_number": row.get(6),
                        "sender_location_number": row.get(6),
                        "sender_asset_number": NULL,
                        "sender_track_id": sender_track_id.strip(),
                        "sender_sku": NULL,
                        "sender_batch_number": NULL,
                        "sender_model_number": NULL,
                        "sender_production_date": row.get(0),
                        "sender_product": NULL,
                        "sender_quantity": row.get(13),
                        "sender_unit": NULL,
                        "receiver_actor_number": RECEIVER_ACTOR_NUMBER,
                        "receiver_location_number": row.get(11),
                        "receiver_asset_number": NULL,
                        "receiver_track_id": row.get(3).strip(),
                        "receiver_sku": NULL,
                        "receiver_batch_number": NULL,
                        "receiver_model_number": NULL,
                        "receiver_production_date": row.get(0),
                        "receiver_product": receiver_product.strip(),
                        "receiver_quantity": row.get(13),
                        "receiver_unit": RECEIVER_UNIT,
                        "tags": NULL,
                    }
                    row_list.append(FactoryReception(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=";")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
