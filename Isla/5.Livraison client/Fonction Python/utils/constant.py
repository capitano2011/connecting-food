from utils.utils import generate_output_filename

NULL = None
CATEGORY = "livraison client"
STEP = "consigning"
ACTOR_SENDER = "53844948900029"
ACTOR_RECEIVER = "Distributeur"

INPUT_FILE_PATH = "/data/blockchainLI - 23_05_22 20_00_02.xlsx"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
