from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Mouture"
STEP = "processing"

INPUT_FILE_PATH = "/data/AXEREAL_Moulin de Reuilly_données de tracabilité Savoir Terre_nouveau format.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
