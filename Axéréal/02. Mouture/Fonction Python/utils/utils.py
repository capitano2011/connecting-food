import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "AXEREAL_Moulin de Reuilly_données de tracabilité Savoir Terre_nouveau format - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
