import os
import pandas as pd
from model.grinding import Grinding
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 36
    if column_number >= 36:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            quantity = (
                str(row.get("Quant. Sortante mouture (kg)"))
                .strip()
                .replace(",", ".")
            )
            # Dict model
            grinding_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date mouture"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("SIRET"),
                "sender_location_number": row.get("Code Moulin"),
                "sender_asset_number": row.get("N° Cellule entrante"),
                "sender_track_id": row.get("N° Lot ble entrant"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date mouture"),
                "sender_product": row.get("Code produit farine"),
                "sender_quantity": quantity,
                "sender_unit": NULL,
                "receiver_actor_number": NULL,
                "receiver_location_number": NULL,
                "receiver_asset_number": row.get("N° Cellule stk farine"),
                "receiver_track_id": row.get("N° lot mouture"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__n°_lot_entrant": row.get("N° Lot ble entrant"),
                "payload__code_produit_entrant": row.get("Code Var ble Stk"),
                "payload__quantité_entrante_(kg)": row.get(
                    "Quant. Ble entrante (kg)"
                ),
                "payload__n°_lot_mouture": row.get("N° lot mouture"),
            }

            row_list.append(Grinding(**grinding_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
