import os
import pandas as pd
from model.agricola import Agricola
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    CATEGORY,
    LEVEL,
    UNIT,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 22
    if column_number >= 22:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            agricola_dict = {
                "category": CATEGORY,
                "step": NULL,
                "level": LEVEL,
                "capture_start": row.get("Data Raccolta"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("AZIENDA"),
                "sender_location_number": row.get("AZIENDA"),
                "sender_asset_number": row.get("Apezzamento d'origine"),
                "sender_track_id": row.get("Lotto"),
                "sender_sku": NULL,
                "sender_batch_number": row.get("Lotto"),
                "sender_model_number": NULL,
                "sender_production_date": row.get("Data Raccolta"),
                "sender_product": row.get("Tipologia Prodotto"),
                "sender_quantity": row.get(
                    "Quantità di basilico\nsostenibile\nritirato (kg)"
                ),
                "sender_unit": UNIT,
                "receiver_actor_number": row.get("AZIENDA"),
                "receiver_location_number": row.get("AZIENDA"),
                "receiver_asset_number": row.get("Apezzamento d'origine"),
                "receiver_track_id": row.get("Lotto"),
                "receiver_sku": NULL,
                "receiver_batch_number": row.get("Lotto"),
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": row.get("Lotto"),
                "payload__foglio": row.get("Foglio\npartic."),
                "payload__n_particella": row.get(
                    "N. particella\ncatastale a\nbasilico\n sostenbile"
                ),
                "payload__kg_semente": row.get(
                    "Kg semente\ncertificata\nusata"
                ),
                "payload__resa_produttiva": row.get(
                    "Resa \nproduttiva\nmedia ton/ha a sfalcio"
                ),
                "payload__data_di_semina": row.get("Data di semina"),
                "payload__varieta": row.get("Varietà"),
                "payload__ numero_di_taglio": row.get("Numero di taglio"),
                "payload__numero_ddt": row.get(
                    "Numero DDT  da Parma Vivai a Barilla"
                ),
                "payload__certificato_iscc_plus": row.get(
                    "Nr. Certificato ISCC PLUS"
                ),
                "payload__iscc_compliant": row.get(
                    "ISCC PLUS, ISCC COMPLIANT"
                ),
                "payload__criteri_sostenibilita": row.get(
                    "Criteri Sostenibilità"
                ),
                "payload__tipo_di_custodia": row.get(
                    "Informazione sul tipo di custodia"
                ),
            }

            row_list.append(Agricola(**agricola_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
