from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Trasformazione"
LEVEL = "2"
BARILLA = "BARILLA"
KG = "kg"
DATA_PRODUZIONE = "Data_Produzione"
GIORNO = "Giorno_CLC1"
INPUT_FILE_PATH = "/data/130_Prd_Rintracciabilita_MP_2.11.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
