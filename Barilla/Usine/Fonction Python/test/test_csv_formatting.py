import os, sys, unittest, json, csv
import pandas as pd

sys.path.append(os.path.abspath(".."))
from main import format_csv


def nan_to_None(s: str):
    return s if s != "nan" else "None"


def remove_brackets(str_input: str):
    return (
        str_input.replace("\\ufeff", "")
        .replace("['", "")
        .replace("']", "")
        .replace("'", "")
        .replace(" ", "")
    )


DATA_PRODUZIONE = "Data_Produzione"
GIORNO = "Giorno_CLC1"
NULL = None
SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))
INPUT_PATH = SOURCE_PATH + "/input"
OUTPUT_PATH = SOURCE_PATH + "/output"
CONF_PATH = SOURCE_PATH + "/conf/conf.json"
input_file_path = INPUT_PATH + "/input.csv"

with open(CONF_PATH) as f:
    conf = json.load(f)


# Open the CSV file for reading the first two lines
with open(input_file_path, newline="", encoding="utf-8") as csvfile:
    # Create a CSV reader object
    reader = csv.reader(csvfile, delimiter=",")

    # Read the first two rows of the file
    row1 = remove_brackets(str(next(reader)))
    row2 = str(next(reader))
    line1 = row1.split(",")
    line2 = eval(row2)


class TestStringMethods(unittest.TestCase):
    def test_format_csv(self):
        # Define input and expected output paths

        output_file_path = OUTPUT_PATH + "/actual_output.csv"

        # Call the function to be tested
        format_csv(source_file_path=input_file_path, output_file_path=output_file_path)

        # Load the input and output files as dataframes
        input_df = pd.read_csv(
            input_file_path, delimiter=",", skiprows=3, encoding="utf-8"
        )

        output_df = pd.read_csv(output_file_path, delimiter=",", encoding="utf-8")

        # Loop input and output line by line
        for (index1, row1), (index2, row2) in zip(
            input_df.iterrows(), output_df.iterrows()
        ):
            for key, value in conf.items():
                if key in [
                    "category",
                    "level",
                    "sender_unit",
                    "receiver_unit",
                    "receiver_actor_number",
                    "receiver_location_number",
                ]:
                    expr1 = str(value).strip()

                elif key in ["capture_start", "sender_production_date"]:
                    expr1 = (
                        line2[line1.index(DATA_PRODUZIONE)]
                        if DATA_PRODUZIONE in line1
                        else NULL
                    )

                elif key in ["sender_track_id", "receiver_track_id"]:
                    expr1 = line2[line1.index(GIORNO)] if GIORNO in line1 else NULL
                else:
                    expr1 = row1.get(value)

                expr2 = row2.get(key)
                try:
                    self.assertEqual(
                        nan_to_None(str(expr1).strip()),
                        nan_to_None(str(expr2).strip()),
                        msg=f"{expr1} != {expr2}",
                    )
                except AssertionError as e:
                    print(f"key:{key} >> erreur : {expr1} != {expr2}")


if __name__ == "__main__":
    unittest.main()
