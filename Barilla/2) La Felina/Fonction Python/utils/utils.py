import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "LaFelina_TracciabilitàAvanzata2022_" + now.strftime("%Y%m%d") + ".csv"
