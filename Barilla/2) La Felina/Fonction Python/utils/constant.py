from utils.utils import generate_output_filename

NULL = None
CATEGORY = "raccolto"
LEVEL = "1"
AGRICOLA = "AZ.AGRICOLA LA FELINA"
KG = "kg"
INPUT_FILE_PATH = "/data/1-LaFelina_TracciabilitàAvanzata2022_240622.xlsx"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
