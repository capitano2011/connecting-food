import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    LEVEL,
    AGRICOLA,
    KG,
    CATEGORY
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_excel(source_file_path)

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 12
    if column_number >= 12:
        print("Formatting...")

        # Get input data line by line

        for index, row in df.iterrows():
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": NULL,
                "level": LEVEL,
                "capture_start": row.get("Data Raccolta"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": AGRICOLA,
                "sender_location_number": AGRICOLA,
                "sender_asset_number": row.get("ID Agricoltore/Campo d'origine"),
                "sender_track_id": row.get("Nr. Lotto"),
                "sender_sku": NULL,
                "sender_batch_number": row.get("Nr. Lotto"),
                "sender_model_number": NULL,
                "sender_production_date": row.get("Data Raccolta"),
                "sender_product": row.get("ID Tipologia Prodotto"),
                "sender_quantity": row.get("Peso "),
                "sender_unit": KG,
                "receiver_actor_number": AGRICOLA,
                "receiver_location_number": AGRICOLA,
                "receiver_asset_number": row.get("ID Agricoltore/Campo d'origine"),
                "receiver_track_id": row.get("Nr. Lotto"),
                "receiver_sku": NULL,
                "receiver_batch_number": row.get("Nr. Lotto"),
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": row.get("Nr. Lotto"),
                "payload__n_particella": row.get(
                    "N. particella\ncatastale a\nbasilico\nsostenbile"
                ),
                "payload__kg_semente": row.get("Kg semente\ncertificata\nusata"),
                "payload__resa_produttiva": row.get(
                    "Resa \nproduttiva\nmedia ton/ha a sfalcio"
                ),
                "payload__numero_di_taglio": row.get("Nr di taglio"),
                "payload__numero_ddt": row.get("DDT"),
                "payload__certificato_iscc_plus": row.get(
                    "Nr Certificato ISCC Plus"
                ),
                "payload__iscc_compliant": row.get("ISCC PLUS, ISCC COMPLIANT"),
                "payload__criteri_sostenibilita": row.get("Criteri Sostenibilità"),
                "payload__tipo_di_custodia": row.get(
                    "Informazione sul tipo di custodia"
                ),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
