from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Alimentation animale Terreos"
STEP = "receiving"
INPUT_FILE_PATH = "/data/FLUX ALIMENTS TEREOS PH 22102020.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
