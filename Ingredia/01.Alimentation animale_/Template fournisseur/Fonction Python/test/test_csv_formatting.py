import os, sys, unittest, json
from datetime import datetime
import pandas as pd

sys.path.append(os.path.abspath(".."))
from main import format_csv


def nan_to_None(s: str):
    return s if s != "nan" else "None"


def date_transform(value: str):
    datetime_object = datetime.strptime(value, "%d/%m/%Y")
    return datetime_object.strftime("%d%Y")


SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))
INPUT_PATH = SOURCE_PATH + "/input"
OUTPUT_PATH = SOURCE_PATH + "/output"
CONF_PATH = SOURCE_PATH + "/conf/conf.json"

with open(CONF_PATH) as f:
    conf = json.load(f)


class TestStringMethods(unittest.TestCase):
    def test_format_csv(self):
        # Define input and expected output paths
        input_file_path = INPUT_PATH + "/input.csv"
        output_file_path = OUTPUT_PATH + "/actual_output.csv"

        # Call the function to be tested
        format_csv(source_file_path=input_file_path, output_file_path=output_file_path)

        # Load the input and output files as dataframes
        input_df = pd.read_csv(
            input_file_path, delimiter=";", decimal=".", encoding="iso-8859-1"
        )
        output_df = pd.read_csv(output_file_path, delimiter=",")

        # Loop input and output line by line
        for (index1, row1), (index2, row2) in zip(
            input_df.iloc[1:].iterrows(), output_df.iterrows()
        ):
            for key, value in conf.items():
                quantity = row1.get(value)
                if value == "quantity":
                    expr1 = str(quantity).replace(",", ".")
                elif key in ["step", "category"]:
                    expr1 = str(value).strip()
                else:
                    expr1 = str(quantity)

                quantity_to_check = row2.get(key)

                expr2 = (
                    str(int(quantity_to_check))
                    if key == "sender_quantity"
                    and str(quantity_to_check)[-1] == "0"
                    and str(quantity_to_check)[-2] == "."
                    else str(quantity_to_check)
                )

                try:
                    self.assertEqual(expr1, expr2, msg=f"{expr1} != {expr2}")
                except AssertionError as e:
                    print(f"key:{key} , erreur : {e}")


if __name__ == "__main__":
    unittest.main()
