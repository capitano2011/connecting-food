import os
import pandas as pd
from model.grinding import Grinding
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 17
    if column_number >= 17:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            grinding_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("flow_date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("actor_sender"),
                "sender_location_number": row.get("site_sender"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("flow_date"),
                "sender_product": row.get("code_product_out"),
                "sender_quantity": row.get("quantity_out"),
                "sender_unit": NULL,
                "receiver_actor_number": row.get("actor_receiver"),
                "receiver_location_number": row.get("site_receiver"),
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("batch_number_out"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__code_blé_entrant": row.get("code_product_in"),
                "payload__quantité_entrante": row.get("quantity_in"),
                "payload__lot_réceptionné_consommé": row.get(
                    "batch_number_in"
                ),
                "payload__lot_mouture": row.get("batch_number_out"),
                "payload__code_fournisseur_lot_blé": row.get("actor_sender"),
                "payload__code_site_fournisseur_de_provenance_du_blé": row.get(
                    "site_sender"
                ),
            }

            row_list.append(Grinding(**grinding_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
