import os, json
import pandas as pd
from model.modelclass import ModelClass
from utils.constant import STEP, INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, CATEGORY

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    row_list = []
    print("Formatting...")

    # Get input data line by line

    for index, row in df.iterrows():
        quantity = str(row.get("QuantiteOF")).replace(",", ".")
        factory_dict = {
            "category": CATEGORY,
            "step": STEP,
            "level": NULL,
            "capture_start": row.get("DateDebutMouture"),
            "capture_end": NULL,
            "capture_uid": NULL,
            "sender_actor_number": row.get("SIRETMoulin"),
            "sender_location_number": NULL,
            "sender_asset_number": NULL,
            "sender_track_id": row.get("CelluleOrigine"),
            "sender_sku": NULL,
            "sender_batch_number": NULL,
            "sender_model_number": NULL,
            "sender_production_date": row.get("DateDebutMouture"),
            "sender_product": row.get("CodeProduit"),
            "sender_quantity": quantity,
            "sender_unit": NULL,
            "receiver_actor_number": row.get("SIRETMoulin"),
            "receiver_location_number": NULL,
            "receiver_asset_number": NULL,
            "receiver_track_id": row.get("NumeroMouture"),
            "receiver_sku": NULL,
            "receiver_batch_number": NULL,
            "receiver_model_number": NULL,
            "receiver_production_date": NULL,
            "receiver_product": NULL,
            "receiver_quantity": NULL,
            "receiver_unit": NULL,
            "tags": NULL,
            "payload__numero_mouture_assemblage": row.get("NumeroMouture"),
            "payload__nom_du_produit": row.get("Produit"),
            "payload__code_site": row.get("CodeSite"),
            "payload__type_de_flux": row.get("TypeFlux"),
            "payload__numero_lot_entrant": row.get("CelluleOrigine"),
        }
        row_list.append(ModelClass(**factory_dict).__dict__)

    # Dataframe transformation
    result = pd.DataFrame(row_list)

    # Dataframe into CSV
    result.to_csv(output_file_path, encoding="utf-8", index=False, sep=",")
    print("Finished")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
