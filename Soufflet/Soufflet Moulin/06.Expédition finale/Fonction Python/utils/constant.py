from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition finale"
STEP = "consigning"

INPUT_FILE_PATH = "/data/20230322 - M02CB - EXPORT - EXPEDITIONS.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
