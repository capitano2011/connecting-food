import os
import pandas as pd
from model.final_expedition import FinalExpedition
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []
    # Check if column number of the input data exceed 20
    if column_number >= 20:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            # Dict model
            final_expedition_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("SiretMoulin"),
                "sender_location_number": row.get("CodeSiteMoulin"),
                "sender_asset_number": NULL,
                "sender_track_id": row.get("LotMoutureExpe"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date"),
                "sender_product": row.get("CodeProduit"),
                "sender_quantity": row.get("Quantite"),
                "sender_unit": row.get("Unite"),
                "receiver_actor_number": row.get("CodeClientDestinataireExpedition"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("NumBLExpe"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_mouture_expédiée": row.get("LotMoutureExpe"),
                "payload__code_produit": row.get("CodeProduit"),
                "payload__vrac_ou_sac": row.get("VracOuSac"),
                "payload__code_site": row.get("CodeSiteMoulin"),
                "payload__cellule_d'_origine": row.get("CelluleOrigine"),
                "payload___type_de_flux": row.get("TypeDeFlux"),
                "payload___heure_de_preparation": row.get("HeurePreparation"),
                "payload___heure_de_depart": row.get("HeureDepart"),
                "payload___code_client": row.get("CodeClientDestinataireExpedition"),
                "payload___numéro_de_BL": row.get("NumBLExpe"),
            }

            row_list.append(FinalExpedition(**final_expedition_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
