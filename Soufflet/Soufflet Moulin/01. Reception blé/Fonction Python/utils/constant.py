from utils.utils import generate_output_filename

NULL = None
INPUT_FILE_PATH = "/data/20230118 - M02LT - EXPORT - RECEPTIONS.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
CATEGORY = "Réception blé"
STEP = "Receiving"
