import os
import pandas as pd
from model.wheat_contribution import WheatContribution
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path,
        delimiter=";",
        encoding="iso-8859-1",
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 37
    if column_number >= 37:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            # Dict model
            wheat_contribution_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("date_mvt"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("ref_actor_from"),
                "sender_location_number": row.get("ref_actor_from"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("date_mvt"),
                "sender_product": row.get("ref_product_c"),
                "sender_quantity": row.get("quantity"),
                "sender_unit": row.get("unit"),
                "receiver_actor_number": row.get("ref_actor_to"),
                "receiver_location_number": row.get("code_site"),
                "receiver_asset_number": row.get("ref_stock_tool"),
                "receiver_track_id": row.get("ref_lot_d"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__site_receveur": row.get("code_site"),
                "payload__sans_insecticide": row.get("sans_insecticide"),
                "payload__code_variété_stockée": row.get(
                    "ref_product_ingredient"
                ),
                "payload__date_mouvement": row.get("date_mvt"),
                "payload__date_annulation": row.get("date_annulation"),
                "payload__numero_de_lot_apport": row.get("ref_lot_d"),
            }

            row_list.append(
                WheatContribution(**wheat_contribution_dict).__dict__
            )

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
