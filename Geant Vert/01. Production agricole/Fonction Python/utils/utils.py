import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "Trac_Agro_" + now.strftime("%Y_%m_%d") + ".csv"
