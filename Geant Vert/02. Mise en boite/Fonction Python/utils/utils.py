import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "Trac_Prod_" + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
