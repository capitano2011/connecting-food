from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Mise en boite"
STEP = "packing"
PRODUCT = "Boite maïs"
ACTOR = "44867070300017"
QUANTITY = "1"

INPUT_FILE_PATH = "/data/Trac_Prod_2022_10_06.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
