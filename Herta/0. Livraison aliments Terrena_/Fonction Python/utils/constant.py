from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Livraison aliments Terrena"
STEP = "receiving"
ACTOR_SENDER = "42970729200018_2"

INPUT_FILE_PATH = "/data/HERTA_20201027_200019.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
