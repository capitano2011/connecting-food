import os
import pandas as pd
from model.removal import Removal
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []
    # # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            # Dict model
            removal_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("date_enlevement_porc"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("actor_from"),
                "sender_location_number": row.get("sitefrom"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("date_enlevement_porc"),
                "sender_product": row.get("code_produit"),
                "sender_quantity": row.get("quantité_envoyée"),
                "sender_unit": NULL,
                "receiver_actor_number": row.get("actor_to"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("lot_envoyé"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__Date_d_abattage": row.get("date_tuerie"),
                "payload__lot": row.get("lot_envoyé"),
            }

            row_list.append(Removal(**removal_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
