import os
import pandas as pd
from model.cutting import Cutting
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []
    # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            lots_entrants = row.get("lot=Date_tuerie+frappe éleveur").split(
                "/"
            )
            quantites_entrantes = row.get("Quantité entrante").split("/")
            quantity = row.get("Quantité produite").lstrip("0")

            # Apply unwind transformation
            for lot_entrant, quantite_entrante in zip(
                lots_entrants, quantites_entrantes
            ):
                # Dict model
                cutting_dict = {
                    "category": CATEGORY,
                    "step": STEP,
                    "level": NULL,
                    "capture_start": row.get("Date de découpe"),
                    "capture_end": NULL,
                    "capture_uid": NULL,
                    "sender_actor_number": row.get("SIRET Abattoir"),
                    "sender_location_number": row.get("Code site Abattoir"),
                    "sender_asset_number": NULL,
                    "sender_track_id": lot_entrant,
                    "sender_sku": NULL,
                    "sender_batch_number": NULL,
                    "sender_model_number": NULL,
                    "sender_production_date": row.get("Date de découpe"),
                    "sender_product": row.get("Code produit sortant"),
                    "sender_quantity": quantity.replace(",", "."),
                    "sender_unit": row.get("unité"),
                    "receiver_actor_number": NULL,
                    "receiver_location_number": NULL,
                    "receiver_asset_number": NULL,
                    "receiver_track_id": row.get("Lot sortant"),
                    "receiver_sku": NULL,
                    "receiver_batch_number": NULL,
                    "receiver_model_number": NULL,
                    "receiver_production_date": NULL,
                    "receiver_product": NULL,
                    "receiver_quantity": NULL,
                    "receiver_unit": NULL,
                    "tags": NULL,
                    "payload__lot_entrant": lot_entrant,
                    "payload__lot_sortant": row.get("Lot sortant"),
                    "payload__code_produit_entrant": row.get(
                        "Code produit entrant"
                    ),
                    "payload__quantité_entrante": quantite_entrante,
                    "payload__unité": row.get("unité"),
                    "payload__unité_produite": row.get("unité.1"),
                    "payload__cahier_des_charges": row.get(
                        "Cahier des charges"
                    ),
                    "payload__n_OF": row.get("OF"),
                }

                row_list.append(Cutting(**cutting_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
