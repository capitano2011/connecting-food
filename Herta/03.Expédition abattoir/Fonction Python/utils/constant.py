from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition abattoir"
STEP = "consigning"
INPUT_FILE_PATH = "/data/03HERTA_BL_02025389_20201029_143501.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
