from utils.utils import generate_output_filename

NULL = None
ACTOR_SENDER = "31104319400621"
CATEGORY = "Transformation"
STEP = "packing"
INPUT_FILE_PATH = "/data/bc_herta_2022_1012_p21_eng.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
