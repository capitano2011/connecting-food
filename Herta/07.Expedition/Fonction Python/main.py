import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, STEP, CATEGORY

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=",", decimal=".", encoding="utf-8")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 15
    if column_number >= 15:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("posting_date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("plant"),
                "sender_location_number": row.get("plant"),
                "sender_asset_number": NULL,
                "sender_track_id": row.get("batch"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("posting_date"),
                "sender_product": row.get("material"),
                "sender_quantity": row.get("qty_in_buom"),
                "sender_unit": NULL,
                "receiver_actor_number": row.get("receiving_plant"),
                "receiver_location_number": row.get("receiving_plant"),
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("batch"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__code_mouvement": row.get("movement_type"),
                "payload__lot_envoyé": row.get("batch"),
                "payload__ordre_de_fabrication": row.get("production_order"),
                "payload__numero_compte_client": row.get("customer_ac_no."),
                "payload__date_de_peremption": row.get("shelf_life_exp._date"),
                "payload__date_de_production_du_lot": row.get("date_of_production_o"),
                "payload__heure_d_entree": row.get("time_of_entry"),
                "payload__code_produit": row.get("material"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
