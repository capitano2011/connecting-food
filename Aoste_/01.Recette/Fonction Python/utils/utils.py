import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "traça_lot_" + now.strftime("%d_%m_%Y") + ".csv"
