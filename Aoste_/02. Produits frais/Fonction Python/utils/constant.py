from utils.utils import generate_output_filename

NULL = None
ACTOR_SENDER = "38881872600100"
ACTOR_RECEIVER = "38881872600100"
CATEGORY = "Produits frais"
STEP = "processing"
INPUT_FILE_PATH = "/data/traça lot 8618040.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
