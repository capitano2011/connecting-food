import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR_SENDER,
    ACTOR_RECEIVER,
    CATEGORY
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", encoding="utf-8")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 17
    if column_number >= 17:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            hierarchie = row.get("Niv. hiérarchie")
            article = row.get("Article")
            quantity = (
                row.get("Quantité")
                .strip()
                .replace(" ", "")
                .replace(",", ".")
                .rstrip("0")
                .rstrip(".")
            )
            order = (
                str(row.get("Ordre"))
                .strip()
                .replace(" ", "")
                .replace(",", ".")
                .rstrip("0")
                .rstrip(".")
            )
            order = NULL if order == "nan" else order
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date de fabrication"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": row.get("Lot"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date de fabrication"),
                "sender_product": row.get("Article"),
                "sender_quantity": quantity,
                "sender_unit": row.get("Unité de qté base"),
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": order,
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__type": row.get("Type"),
                "payload__article": article,
                "payload__désignation_art": row.get("Désignation art."),
                "payload__Division": row.get("Division"),
                "payload__Lot": row.get("Lot"),
                "payload__unité_de_quantité_de_base": row.get("Unité de qté base"),
                "payload__ordre": order,
                "payload__date_de_fabrication": row.get("Date de fabrication"),
                "payload__date_peremption_dlc": row.get("Date péremption/DLC"),
                "payload__niv_hierarchie": hierarchie,
                "payload__niv_hierarchie_+_article": f"{hierarchie}_{article}",
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
