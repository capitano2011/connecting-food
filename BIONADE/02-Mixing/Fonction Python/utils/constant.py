from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Mixing"
STEP = "processing"
ACTOR = "DE 228 256 901"
SITE = "B01"
INPUT_FILE_PATH = "/data/Holunder Sirup FR_5vweQ9J.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
