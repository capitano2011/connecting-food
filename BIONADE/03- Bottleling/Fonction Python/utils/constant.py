from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Bottleling"
STEP = "packing"
ACTOR = "DE 228 256 901"

INPUT_FILE_PATH = "/data/Holunder Abfüllung für FR_QP1NWaE.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
