import os
import pandas as pd
import glob
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    # Find all CSV files in the source_file_path
    csv_files = glob.glob(source_file_path + "*.csv")

    print("Formatting...")

    for csv_file in csv_files:
        filename = str(csv_file).split("/").pop()

        df = pd.read_csv(csv_file, delimiter=";")

        # column update
        df["DATA ENTRATA MERCE"] = df["DATA ENTRATA MERCE"].replace(
            "0", pd.NaT
        )
        df["DATA ENTRATA MERCE"] = pd.to_datetime(
            df["DATA ENTRATA MERCE"], format="%Y%m%d", errors="coerce"
        ).dt.strftime("%d/%m/%Y")
        df["DATA ENTRATA MERCE"] = df["DATA ENTRATA MERCE"].replace(
            pd.NaT, "0"
        )

        # Dataframe into CSV
        df.to_csv(
            output_file_path + "Output-" + filename,
            sep=";",
            index=False,
            encoding="utf-8",
        )

    print("Finished")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
