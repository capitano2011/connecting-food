import os
import pandas as pd
from model.factory_delivery import FactoryDelivery
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            factory_delivery_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("delivery_at"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("ref_actor_producer"),
                "sender_location_number": row.get("ref_site"),
                "sender_asset_number": NULL,
                "sender_track_id": row.get("ref_delivery_form_display"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("delivery_at"),
                "sender_product": row.get("ref_product"),
                "sender_quantity": row.get("quantity"),
                "sender_unit": row.get("unit"),
                "receiver_actor_number": row.get("ref_actor_receipt"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("ref_lot"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__heure": row.get("hour"),
                "payload__lot_abattage_livré": row.get("ref_lot"),
            }

            row_list.append(FactoryDelivery(**factory_delivery_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
