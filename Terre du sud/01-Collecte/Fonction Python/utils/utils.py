import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "connectingfood_tds_sansogm_collecte_" + now.strftime("%d_%m_%Y") + ".csv"
