from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Abattage"
STEP = "processing"

INPUT_FILE_PATH = (
    "/data/connectingfood_tds_sansogm_abattageSTL_03_04_2023_23 _48_58_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
