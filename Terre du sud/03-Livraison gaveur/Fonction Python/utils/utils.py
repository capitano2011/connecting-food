import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "connectingfood_tds_sansogm_livraison1_" + now.strftime("%d_%m_%Y") + ".csv"
