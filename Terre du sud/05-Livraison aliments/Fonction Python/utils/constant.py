from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Livraison aliments"
STEP = "receiving"
INPUT_FILE_PATH = (
    "/data/connectingfood_tds_sansogm_livraison2_03_03_2023_23 _47_43_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
