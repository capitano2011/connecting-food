from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Miel"
STEP = "processing"
PRODUCT = "Miel en pot JUSTE & VENDEEN"
ACTOR_RECEIVER = "35110319700015"

INPUT_FILE_PATH = "/data/20201203_-_JUSTE_fichier_flux_dinfos_miel_sTmiNkK.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
