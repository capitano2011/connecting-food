from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Farine"
STEP = "processing"
PRODUCT = "Farine Juste"
INPUT_FILE_PATH = "/data/20220214_-_FARINE_juste.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
