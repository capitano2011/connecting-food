from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Oeuf"
STEP = "processing"
PRODUCT = "Boite de 6 œufs plein air JUSTE & VENDEEN"
ACTOR = "35361070200022"

INPUT_FILE_PATH = "/data/20230405 - Traçabilite JUST ET VENDEEN 05042023).csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
