import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20230123_-_JUSTE__VENDEEN_fichier_traca_collecte_du_20230116_modifie - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
