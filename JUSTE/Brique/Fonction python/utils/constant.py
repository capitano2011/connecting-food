from utils.utils import generate_output_filename

NULL = None
PRODUCT = "Lait Demi \u00e9cr\u00e9m\u00e9 1L JUSTE de LA"
CATEGORY = "Brique"
INPUT_FILE_PATH = "/data/20220826 - Fichier traça collecte du 20220823.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
STEP = "processing"