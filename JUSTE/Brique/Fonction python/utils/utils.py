import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%d%m%Y") + " - Fichier traça collecte.csv"
