import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        now.strftime("%m%Y%d") + " - FROMAGE JUSTE Tracabilité Collecte du 20210706.csv"
    )
