## INSTALLATION DU PROJET

- Aller dans le répertoire racine du projet, puis créez un virtualenv avec la commande :

```
python3.x -m venv venv

```

- Activez le virtualenv avec la commande :

```
source venv/bin/activate

```

- Installez les dépendances du projet avec la commande :

```
pip install -r requirements.txt

```

- Lancer le script avec :

```
python main.py

```

## RESULTAT

- Vous pouvez voir le fichier csv résultant dans le dossier output

## REMARQUES

- Le fichier source est déja contenu dans le dossier `data`
- Le fichier csv dans `output` aura comme délimiteur un point virgule `,`
