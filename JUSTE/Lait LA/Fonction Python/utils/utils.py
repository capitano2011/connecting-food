import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        now.strftime("%Y%m%d") + " - Fichier traça JUSTE de LA Collecte du 20230109.csv"
    )
