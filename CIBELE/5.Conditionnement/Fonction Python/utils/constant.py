from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Conditionnement"
STEP = "packing"
ACTOR = "39198157800035"
SITE = "2930"
INPUT_FILE_PATH = "/data/20220712 Conditionnement.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
