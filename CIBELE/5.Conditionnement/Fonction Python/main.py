import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR,
    SITE,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path, delimiter=";", decimal=".", encoding="iso-8859-1"
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 10
    if column_number >= 10:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            annee_de_recolte = row.get("Ann\u00e9e de r\u00e9colte")

            annee_de_recolte = (
                str(annee_de_recolte).replace("1", "2021")
                if "1" in str(annee_de_recolte)
                else str(annee_de_recolte)
            )
            poids = row.get("Poids ( Kg )").strip().replace(",", ".")
            numero_lot = str("0000000" + str(row.get("N\u00b0 lot paquet")))

            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": SITE,
                "sender_asset_number": row.get("N\u00b0 Container"),
                "sender_track_id": row.get("N\u00b0 Container"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date"),
                "sender_product": row.get("Code produit conditionn\u00e9"),
                "sender_quantity": poids,
                "sender_unit": NULL,
                "receiver_actor_number": NULL,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": numero_lot,
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__numero_lot_paquet": numero_lot,
                "payload__année_de_récolte": annee_de_recolte,
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
