from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Récéption vétérinaire"
STEP = "receiving"

INPUT_FILE_PATH = "/data/PAE4-DataV0.3-Veto_xjYofVi.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
