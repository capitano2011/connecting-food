from utils.utils import generate_output_filename

NULL = None
ACTOR_SENDER = "63542032600020"
CATEGORY = "Récéption aliments"
STEP = "receiving"
INPUT_FILE_PATH = "/data/2205-ANALYSE_LIVRAISON_PORC-ARMORv10all_QACDXcx.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
