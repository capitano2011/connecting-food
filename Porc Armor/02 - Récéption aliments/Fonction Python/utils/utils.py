import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%d%m") + "_ANALYSE_LIVRAISON_PORC-ARMORv10all_QACDXcx" + ".csv"


def decimal_transformation(value: str):
    if "," in value:
        quantity = value.strip().replace(",", ".").rstrip("0").rstrip(".")
    else:
        quantity = value

    return quantity
