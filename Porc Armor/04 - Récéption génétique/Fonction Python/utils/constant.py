from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Récéption génétique"
STEP = "receiving"

INPUT_FILE_PATH = "/data/Extraction_BDPorc_modifiée_v400_KylGQtt.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
