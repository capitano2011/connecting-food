import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Extraction_BDPorc_modifiée_v400_KylGQtt - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
