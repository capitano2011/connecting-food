import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%d.%m.%Y.") + "Daco_Blockchain_-_Packing_DataV6_298_fin.csv"
