from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Sizing Montevista"
STEP = "Inspecting"
SENDER_ACTOR = "USPOMONA"
RECEIVER_ACTOR = "USMONTEVISTA"
SITE = "PACKER"

INPUT_FILE_PATH = "/data/Daco_Blockchain_-_Sizing_V9_E7_634-651_5IwOyll.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
