import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Daco_Blockchain_-_Sizing_V9_E7_634-651_5IwOyll - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
