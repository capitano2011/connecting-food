import os, sys, unittest, json
import pandas as pd

sys.path.append(os.path.abspath(".."))
from main import format_csv


def nan_to_None(s: str):
    return s if s != "nan" else "None"


def lowercase_except_first(text):
    return text[0] + text[1:].lower()


SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))
INPUT_PATH = SOURCE_PATH + "/input"
OUTPUT_PATH = SOURCE_PATH + "/output"
CONF_PATH = SOURCE_PATH + "/conf/conf.json"

with open(CONF_PATH) as f:
    conf = json.load(f)


class TestStringMethods(unittest.TestCase):
    def test_format_csv(self):
        # Define input and expected output paths
        input_file_path = INPUT_PATH + "/input.csv"
        output_file_path = OUTPUT_PATH + "/actual_output.csv"

        # Call the function to be tested
        format_csv(source_file_path=input_file_path, output_file_path=output_file_path)

        # Load the input and output files as dataframes
        input_df = pd.read_csv(
            input_file_path, delimiter=";", decimal=".", encoding="utf-8"
        )

        output_df = pd.read_csv(output_file_path, delimiter=",")

        # Loop input and output line by line
        for (index1, row1), (index2, row2) in zip(
            input_df.iterrows(), output_df.iterrows()
        ):
            for key, value in conf.items():
                if key in [
                    "step",
                    "category",
                    "sender_actor_number",
                    "sender_quantity",
                    "receiver_actor_number",
                    "receiver_location_number",
                ]:
                    expr1 = str(value).strip()
                    expr2 = str(row2.get(key))
                elif key in ["sender_product", "payload__variety"]:
                    expr1 = str(row1.get(value))

                    if expr1 in [
                        "NONPAREIL",
                        "ALDRICH",
                        "MONTEREY",
                        "SONORA",
                        "CARMEL",
                        "FRITZ",
                    ]:
                        expr1 = lowercase_except_first(expr1)
                    elif expr1 == "WOOD COLONY":
                        expr1 = "Wood Colony"

                    expr2 = str(row2.get(key))

                elif key == "payload__acre_feet_ratio":
                    block = row1.get("Block")

                    if block in ["Holthouse", "Fund II 312", "Fund II 763"]:
                        expr1 = "3,45"
                    elif block in [
                        "Olive",
                        "321",
                        "Fund II 160",
                        "Fund II 376",
                        "611",
                        "Ardis 640",
                    ]:
                        expr1 = "3,16"
                    elif block == "Crabtree":
                        expr1 = "3,54"
                    elif block == "Grohl 626":
                        expr1 = "3,10"
                    elif block == "Tim Bell":
                        expr1 = "3,47"
                    elif block == "Warnerville 369":
                        expr1 = "3,48"
                    expr2 = str(row2.get(key))

                elif key in ["receiver_track_id", "payload__batch_number"]:
                    shipping_year = row1.get("Shipping Year")
                    block = row1.get("Block")
                    variety = row1.get("Variety")

                    if variety in [
                        "NONPAREIL",
                        "ALDRICH",
                        "MONTEREY",
                        "SONORA",
                        "CARMEL",
                        "FRITZ",
                    ]:
                        variety = lowercase_except_first(variety)
                    elif variety == "WOOD COLONY":
                        variety = "Wood Colony"

                    expr1 = f"{block}-{shipping_year}-{variety}"
                    expr2 = str(row2.get(key))

                else:
                    expr1 = str(row1.get(value))
                    expr2 = str(row2.get(key))

                try:
                    self.assertEqual(
                        nan_to_None(str(expr1).strip()),
                        nan_to_None(str(expr2).strip()),
                        msg=f"{expr1} != {expr2}",
                    )
                except AssertionError as e:
                    print(f"key:{key} >> erreur : {expr1} != {expr2}")


if __name__ == "__main__":
    unittest.main()
