import os
import pandas as pd
from model.modelclass import Model_class
from utils.utils import lowercase_except_first
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR_SENDER,
    ACTOR_RECEIVER,
    SITE_RECEIVER,
    QUANTITY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", encoding="utf-8")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 5
    if column_number >= 5:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            shipping_year = row.get("Shipping Year")
            block = row.get("Block")
            variety = row.get("Variety")
            acre_feet_ratio = ""

            if block in ["Holthouse", "Fund II 312", "Fund II 763"]:
                acre_feet_ratio = "3,45"
            elif block in [
                "Olive",
                "321",
                "Fund II 160",
                "Fund II 376",
                "611",
                "Ardis 640",
            ]:
                acre_feet_ratio = "3,16"
            elif block == "Crabtree":
                acre_feet_ratio = "3,54"
            elif block == "Grohl 626":
                acre_feet_ratio = "3,10"
            elif block == "Tim Bell":
                acre_feet_ratio = "3,47"
            elif block == "Warnerville 369":
                acre_feet_ratio = "3,48"

            if variety in [
                "NONPAREIL",
                "ALDRICH",
                "MONTEREY",
                "SONORA",
                "CARMEL",
                "FRITZ",
            ]:
                variety = lowercase_except_first(variety)
            elif variety == "WOOD COLONY":
                variety = "Wood Colony"

            batch_number = f"{block}-{shipping_year}-{variety}"

            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Harvest Start Date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": block,
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Harvest Start Date"),
                "sender_product": variety,
                "sender_quantity": QUANTITY,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": SITE_RECEIVER,
                "receiver_asset_number": NULL,
                "receiver_track_id": batch_number,
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__shipping_year": shipping_year,
                "payload__ranch": row.get("Ranch"),
                "payload__batch_number": batch_number,
                "payload__variety": variety,
                "payload__block": block,
                "payload__acre_feet_ratio": acre_feet_ratio,
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
