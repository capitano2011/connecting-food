from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Growing Pomona"
STEP = "commissioning"
ACTOR_SENDER = "30048291600032"
QUANTITY = "1"
ACTOR_RECEIVER = "USMONTEVISTA"
SITE_RECEIVER = "PACKER"
INPUT_FILE_PATH = "/data/growing_v2023_VU7dB3n.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
