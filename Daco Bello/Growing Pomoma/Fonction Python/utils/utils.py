def generate_output_filename():
    return "growing_v2023_VU7dB3n.csv"


def lowercase_except_first(text):
    return text[0] + text[1:].lower()
