import os
import pandas as pd
from model.packaging import Packaging
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", encoding="iso-8859-1")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 23
    if column_number >= 23:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            CV = f'Chrono VIF:{row.get("Chrono")}'
            quantity = row.get("Quantite").replace("-", "").replace(",", ".")
            TX_OXY = (
                str(row.get("TX OXY")).replace(",", ".")
                if str(row.get("TX OXY")).replace(",", ".") != "nan"
                else NULL
            )

            # Dict model
            packaging_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date mvt"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": row.get("Lot"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date mvt"),
                "sender_product": row.get("Article"),
                "sender_quantity": quantity,
                "sender_unit": row.get("Unnamed: 8"),
                "receiver_actor_number": ACTOR,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Chrono"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_de_réception": row.get("Lot"),
                "payload__taux_oxygène_résiduel": TX_OXY,
                "payload__libellé_article": row.get("Libelle article"),
                "payload__dép": row.get("Dep"),
                "payload__ressource_primaire": row.get("Ressource primaire"),
                "payload__heure_du_mouvement": row.get("Heure du mouvement"),
                "payload__chrono_VIF": row.get("Chrono"),
                "payload__CV": CV,
                "payload__nature": row.get("Nature"),
                "payload__DLUO": row.get("DLUO"),
            }

            row_list.append(Packaging(**packaging_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
