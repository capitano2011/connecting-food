import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR_SENDER,
    ACTOR_RECEIVER,
    SITE_SENDER,
    PRODUCT,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""
    df = pd.read_csv(source_file_path, delimiter=",", encoding="utf-8")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("DepartureDate"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": SITE_SENDER,
                "sender_asset_number": NULL,
                "sender_track_id": row.get("PackingWONumber"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("DepartureDate"),
                "sender_product": PRODUCT,
                "sender_quantity": row.get("NetWeight"),
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("PackingWONumber"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__identifiant_de_palette": row.get("FinishedGoodsTagNumber"),
                "payload__lot_de_packaging": row.get("PackingWONumber"),
                "payload__carctéristique_du_lot": row.get("ItemDesc"),
                "payload__USDA_grade": row.get("GradeDesc"),
                "payload__calibre": row.get("SizeDesc"),
                "payload__variété": row.get("VarietyDesc"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
