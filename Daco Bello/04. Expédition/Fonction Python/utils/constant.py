from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition"
STEP = "consigning"
ACTOR = "30048291600032"
INPUT_FILE_PATH = "/data/20230403_060023_dacobello_blockchain.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
