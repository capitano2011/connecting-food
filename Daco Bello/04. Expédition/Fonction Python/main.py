import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", encoding="iso-8859-1")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 23
    if column_number >= 23:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            quantity = (
                str(row.get("Quantite"))
                .strip()
                .replace(",", ".")
                .replace("-", "")
                .rstrip("0")
                .rstrip(".")
            )
            chrono_str = str(row.get("Chrono"))
            chrono = f"Chrono VIF:{chrono_str}"
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date mvt"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": row.get("Lot"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date mvt"),
                "sender_product": row.get("Article"),
                "sender_quantity": quantity,
                "sender_unit": NULL,
                "receiver_actor_number": row.get("Client"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Chrono"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_de_conditonnement": row.get("Lot"),
                "payload__libellé_article": row.get("Libelle article"),
                "payload__dép": row.get("Dep"),
                "payload__ressource_primaire": row.get("Ressource primaire"),
                "payload__heure_du_mouvement": row.get("Heure du mouvement"),
                "payload__chrono_vif": row.get("Chrono"),
                "payload__cv": chrono,
                "payload__nature": row.get("Nature"),
                "payload__DLUO": row.get("DLUO"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
