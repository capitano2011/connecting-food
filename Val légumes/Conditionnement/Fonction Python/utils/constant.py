from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Conditionnement"
STEP = "Commissioning"
ACTOR = "38848978300018"
QUANTITY = 0
SITE_RECEIVER = "VAL LEGUMES"

INPUT_FILE_PATH = "/data/Lots 2022_23.08.2022 (version4)_ZIZWBlr.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
